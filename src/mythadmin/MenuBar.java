package mythadmin;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import java.awt.event.*;

public class MenuBar extends JMenuBar {
	public MenuBar () {
		ActionListener printListener = new ActionListener () {
			public void actionPerformed(ActionEvent event) {
				System.out.println(Messages.getString("MenuBar.temp.0") + event.getActionCommand () + Messages.getString("MenuBar.temp.1")); //$NON-NLS-1$ //$NON-NLS-2$
				if (event.getActionCommand().equals(Messages.getString("MenuBar.about"))) {
					AboutWindow aboutWindow = new AboutWindow();
				}
			}
		};
		JMenu helpMenu = new JMenu(Messages.getString("MenuBar.help")); //$NON-NLS-1$
		  JMenuItem helpAbout = new JMenuItem(Messages.getString("MenuBar.about"), KeyEvent.VK_A); //$NON-NLS-1$
		  helpAbout.addActionListener(printListener);
		  helpAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));
		  helpMenu.add(helpAbout);
		this.add(helpMenu);
	}
}
