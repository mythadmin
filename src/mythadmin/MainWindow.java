package mythadmin;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import javax.swing.JFrame;

public class MainWindow extends JFrame {

	public MainWindow() throws HeadlessException {
		this.getContentPane().setLayout(null);
		this.initWindow();
	}

	protected void initWindow() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setJMenuBar(new MenuBar());
		this.pack();
		this.setVisible(true);
		
	}
}
