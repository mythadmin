package mythadmin;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.UIManager;

import java.awt.*;


public class AboutWindow extends JFrame {
    JLabel htmlText;
    
    String aboutText = "<html>\n"
    	+ "<h1>mythAdmin</h1>\n"
    	+ "<p><i>An admin tool for MythTV and MythVideo</i></p>\n"
    	+ "<p>Under <b>heavy</b> developement, git repository under<br>\n"
    	+ "<a href=\"http://repo.or.cz/r/mythadmin.git\">http://repo.or.cz/r/mythadmin.git</a><br>\n"
    	+ "or <a href=\"git://repo.or.cz/mythadmin.git\">git://repo.or.cz/mythadmin.git</a>\n"   		
    	+ "</html>";
	public AboutWindow () { 
		htmlText = new JLabel(aboutText);
		htmlText.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setPreferredSize(new Dimension(300,200));
		this.add(htmlText);
		UIManager.put("swing.boldMetal", Boolean.FALSE);
		this.pack();
		this.setVisible(true);
	}
}
